const secrets		= require("./bot_secrets.json");
const config		= require("./config.json");

const Discord		= require('discord.js');
const logger		= require("./logger.js");
const mysql         = require('mysql2/promise');

const db_table      = "mod_ticket"

const mysq_data = {
    host:       secrets.mysql.host,
    port:       secrets.mysql.port,  
    user:       secrets.mysql.user + "2",
    password:   secrets.mysql.pass,
    database:   secrets.mysql.db
}

function log_info(module, submodule, func, msg){
	message = `${msg}`.replace(/(\r\n|\n|\r)/gm, " ")
    console.log(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
    logger.info(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
}

function log_warn(module, submodule, func, msg){
	message = `${msg}`.replace(/(\r\n|\n|\r)/gm, " ")
    console.log(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
    logger.warn(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
}

function log_error(module, submodule, func, msg){
	message = `${msg}`.replace(/(\r\n|\n|\r)/gm, " ")
    console.log (`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
    logger.error(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
}

// Establish Discord Connection
const client  = new Discord.Client({ intents: [Discord.GatewayIntentBits.Guilds, ] })
client.login(secrets.discord)

client.on('ready', bot => {
	log_info('PL-Ticketing', 'client.on', 'ready', 'Online')
});

client.on('warn', warn => {
	log_warn('PL-Ticketing', 'client.on', 'warn', warn.stack)
});

client.on('error', err => {
	log_error('PL-Ticketing', 'client.on', 'error', err.stack)
});


client.on('interactionCreate', async interaction => {
	if (!interaction.isCommand() && !interaction.isButton()){return}
    
    try {
        // Create Thread on button press

        if(interaction.isButton()){
            if(interaction.customId != "ticket_system"){return}

            // Permission Check
            if(interaction.member.roles.cache.has("1169455759566852177") == false){
                interaction.reply({ content:"You do need the Saftey Role to use this Feature", ephemeral: true }).catch(error => {log_error('PL-Ticketing', '', 'interaction reply', error.stack)})                
                return
            } 

            var mysql_conn = await  mysql.createConnection(mysq_data);
            const [results, fields] = await mysql_conn.execute('SELECT id, message FROM ' + db_table + ' WHERE guild = ? AND channel = ? ', [interaction.guildId, interaction.channelId])
            
            if(results.length > 0)
            {
                interaction.reply({ content: 'A private thread got created. You will get access shortly!', ephemeral: true  }).catch(error => {
                    log_error('PL-Ticketing', '', 'button interaction reply', error.stack)
                })
        
                thread_opts = Object.create(config[interaction.guildId].module.ticketing.threadopts)
                thread_opts.name = thread_opts.name + interaction.member.user.username

                interaction.channel.threads.create(thread_opts).then(threadChannel => {
                    threadChannel.send(results[0]['message'].replaceAll("{user}", `<@${interaction.user.id}>`).replaceAll("{n}", `\n`).replaceAll("{t}", `\n`)).catch(error => {
                        log_error('PL-Ticketing', '', 'thread message send', error.stack)
                    })
                })
                .catch(error => {
                    log_error('PL-Ticketing', '', 'ticket thread create', error.stack)
                });
            }
            else
            {
                interaction.reply({ content:"Ticket System is not operating on this channel.", ephemeral: true }).catch(error => {
                    log_error('PL-Ticketing', '', 'interaction reply', error.stack)
                })  
                return
            }


            mysql_conn.end()
        }

        // Handle Slash Commands
        if(interaction.commandName != "ticket_system"){return}

        // Permission Check
        if(interaction.member.roles.cache.has(config[interaction.guildId].common.roles.admin) == false){
            interaction.reply({ content:"You do not have the permissions to use this command", ephemeral: true }).catch(error => {
                log_error('PL-Ticketing', '', 'interaction reply', error.stack)
            })                
            return
        }    

        let btn_emoji   = interaction.options.getString ('btn_emoji')
        let btn_msg     = interaction.options.getString ('btn_msg')
        let message     = interaction.options.getString ('message')
        let channel     = interaction.options.getChannel('channel')
        let n_chan      = interaction.options.getChannel('new_channel')

        if(interaction.options.getSubcommand() == 'create'){

            if(message == null){
                interaction.reply({ content:"Message must not be empty", ephemeral: true }).catch(error => {
                    log_error('PL-Ticketing', '', 'interaction reply', error.stack)
                })                
                return            
            } 

            if(channel == null){
                channel = interaction.channel
            }

            var mysql_conn = await  mysql.createConnection(mysq_data);
            const [results, fields] = await mysql_conn.execute('SELECT COUNT(*) FROM ' + db_table + ' WHERE guild = ? AND channel = ? ', [interaction.guildId, channel.id])

            if(results[0]['COUNT(*)'] < 1)
            {

                const [results_insert, fields_insert] = await mysql_conn.execute('INSERT INTO ' + db_table + ' (guild, channel, message) VALUES (?,?,?)', [interaction.guildId, channel.id, message])

                interaction.reply({ content:"Ticket System creation  was  successfull.", ephemeral: true }).catch(error => {
                    log_error('PL-Ticketing', '', 'interaction reply', error.stack)
                })                      

            }
            else
            {
                interaction.reply({ content:"Ticket System is already operating on this channel.", ephemeral: true }).catch(error => {
                    log_error('PL-Ticketing', '', 'interaction reply', error.stack)
                })  
                return                
            }

            mysql_conn.end()
        }
        else if(interaction.options.getSubcommand() == 'remove'){

            if(channel == null){
                channel = interaction.channel
            }

            var mysql_conn = await  mysql.createConnection(mysq_data);
            const [results, fields] = await mysql_conn.execute('SELECT id FROM ' + db_table + ' WHERE guild = ? AND channel = ? ', [interaction.guildId, channel.id])

            if(results.length > 0)
            {
                const [results_rem, fields_rem] = await mysql_conn.execute('DELETE FROM ' + db_table + ' WHERE id = ?', [results[0]['id']]) 

                interaction.reply({ content:"Ticket System  was  successfully removed.", ephemeral: true }).catch(error => {
                    log_error('PL-Ticketing', '', 'interaction reply', error.stack)
                })

            }
            else
            {
                interaction.reply({ content:"Ticket System is not operating on this channel.", ephemeral: true }).catch(error => {
                    log_error('PL-Ticketing', '', 'interaction reply', error.stack)
                })  
                return
            }

            mysql_conn.end()


        }
        else if(interaction.options.getSubcommand() == 'update'){

            if(message == null && channel == null && n_chan == null ){
                interaction.reply({ content:"No parameter was specified", ephemeral: true }).catch(error => {
                    log_error('PL-Ticketing', '', 'interaction reply', error.stack)
                })                
                return            
            } 

            if(channel == null){
                channel = interaction.channel
            }

            if(n_chan == null){
                n_chan = channel
            }


            var mysql_conn = await  mysql.createConnection(mysq_data);

            const [results, fields] =  await mysql_conn.execute('SELECT id, message FROM ' + db_table + ' WHERE guild = ? AND channel = ? ', [interaction.guildId, channel.id])

            if(results.length > 0)
            {
                if(message == null){
                    message = results[0]['message']
                }    

                const [results_update, fields_update] =  await mysql_conn.execute('UPDATE ' + db_table + ' SET channel = ?, message = ? WHERE id = ?', [n_chan.id, message, results[0]['id']])

                interaction.reply({ content:"Ticket System  was  successfully updated.", ephemeral: true }).catch(error => {
                    log_error('PL-Ticketing', '', 'interaction reply', error.stack)
                })


            }
            else
            {
                interaction.reply({ content:"Ticket System is not operating on this channel.", ephemeral: true }).catch(error => {
                    log_error('PL-Ticketing', '', 'interaction reply', error.stack)
                })  
                return
            }

            mysql_conn.end()
        }
        else if(interaction.options.getSubcommand() == 'button'){

            if(btn_msg == null){
                interaction.reply({ content:"Button text was not specified.", ephemeral: true }).catch(error => {
                    log_error('PL-Ticketing', '', 'interaction reply', error.stack)
                })                
                return  
            }

            if(channel == null){
                channel = interaction.channel
            }

            var mysql_conn = await  mysql.createConnection(mysq_data);


            const [results, fields] =  await mysql_conn.execute('SELECT id FROM ' + db_table + ' WHERE guild = ? AND channel = ? ', [interaction.guildId, channel.id])

            if(results.length > 0)
            {
                button     = new Discord.ButtonBuilder()
                    .setCustomId(`ticket_system`)
                    .setLabel(btn_msg)
                    .setStyle(Discord.ButtonStyle.Primary)
                
                if(btn_emoji != null){button.setEmoji(btn_emoji)}
                
                action_row = new Discord.ActionRowBuilder().addComponents(button)

                interaction.reply({ content:"Button Created", ephemeral: true }).catch(error => {
                    log_error('PL-Ticketing', '', 'interaction reply', error.stack)
                })

                channel.send({components: [action_row]}).catch(error => {
                    log_error('PL-Ticketing', '', 'create button', error.stack)
                })	
            }
            else
            {
                interaction.reply({ content:"Ticket System is not operating on this channel.", ephemeral: true }).catch(error => {
                    log_error('PL-Ticketing', '', 'interaction reply', error.stack)
                })  
                return
            }

            mysql_conn.end()
        }
        else {
            interaction.reply({ content:"Unsupported Command", ephemeral: true }).catch(error => {
                log_error('PL-Ticketing', '', 'interaction reply', error.stack)
            })
            log_warn('PL-Ticketing', '', 'unsupported interaction command', interaction)    
        }

    } catch (error){
        log_error('PL-Ticketing', '', 'interaction create', error.stack)
    }
});
