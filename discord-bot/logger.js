const log_conf	= require("./bot_secrets.json");
const https     = require('https')

class api_logger {

    constructor() {
        this.req_opts = {
            host:       log_conf.logger.host,
            port:       log_conf.logger.port,
            path:       log_conf.logger.path,
            method:     "POST",
            rejectUnauthorized: false,
            headers:    {
                "Content-Type": "application/json",
                "Auth-Token":   log_conf.logger.token
            }
        }
    }

    api_call(level, message){
        var post_req = https.request(this.req_opts, (res) => {
            if(res.statusCode > 300){
                res.on('data', (d) => {
                    console.log("[Error][Logging] "+ res.statusMessage + ": " + d.slice(0, -1));
                });
            }
        });

        var obj         = JSON.parse(message);
        obj["app"]      = "PL Botto"
        obj["tsp"]      = Date.now()
        obj["level"]    = level

        post_req.write(JSON.stringify(obj));
        post_req.on('error', (e) => {
            console.error(e);
        });
        post_req.end(); 
    }

    info(msg) {this.api_call('info',    msg)}
    warn(msg) {this.api_call('warning', msg)}
    error(msg){this.api_call('error',   msg)}

}
const logger = new api_logger()
module.exports = logger
